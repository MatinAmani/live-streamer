// elements
const videoJS = videojs('vid', {
    userActions: {
        hotkeys: true,
    },
})
const container = document.getElementById('container')
const video = document.getElementById('vid')
const controls = document.getElementById('ctrl')
const url = document.getElementById('url').src
const urlParts = url.split('/')
const playIcon = document.getElementById('play-icon')
const pauseIcon = document.getElementById('pause-icon')
const volumeSlider = document.getElementById('volume-slider')
const volumeIconOn = document.getElementById('volume-icon-on')
const volumeIconOff = document.getElementById('volume-icon-off')
const timer = document.getElementById('timer')
const qualityButton = document.getElementById('q-btn')
const qualityList = document.getElementById('q-list')
const qualityOptions = document.getElementsByClassName('q-option')
var qualityListShowState = false
// TODO const qualityOptions = document.querySelectorAll('.q-options')

// autoplays the stream on load
videoJS.autoplay('mute')
var playState = true
videoJS.volume(volumeSlider.value)

// timer for video
var passedTime = 0
var seconds
var minutes
setInterval(() => {
    playState ? passedTime++ : passedTime
    updateTimer(passedTime)
}, 1000)

// functions
function toggleStream() {
    if (playState) {
        videoJS.pause()
        playState = false
    } else {
        videoJS.play()
        playState = true
    }
    updatePlayIcon()
}

function updatePlayIcon() {
    if (playState) {
        playIcon.style.display = 'none'
        pauseIcon.style.display = 'inline'
    } else {
        playIcon.style.display = 'inline'
        pauseIcon.style.display = 'none'
    }
}

function updateVolume({ target }) {
    videoJS.volume(target.value)
}

function updateTimer(time) {
    let sec = time % 60
    let min = parseInt(time / 60)
    let result =
        (min <= 9 ? `0${min}` : `${min}`) +
        ':' +
        (sec <= 9 ? `0${sec}` : `${sec}`)
    timer.innerHTML = result
    playStateCheck()
}

function playStateCheck() {
    // TODO
}

function toggleQualityList() {
    if (qualityListShowState) {
        qualityList.style.display = 'none'
        qualityListShowState = false
    } else {
        qualityList.style.display = 'flex'
        qualityListShowState = true
    }
}

function toggleControls() {
    controls.classList.toggle('show')
}

function updateQuality() {
    //TODO

    toggleQualityList()
}

// adding functions to elements
// video.addEventListener('click', toggleStream)
container.addEventListener('mouseenter', toggleControls)
container.addEventListener('mouseleave', toggleControls)
playIcon.addEventListener('click', toggleStream)
pauseIcon.addEventListener('click', toggleStream)
volumeSlider.addEventListener('input', updateVolume)
qualityButton.addEventListener('click', toggleQualityList)
for (let i = 0; i < qualityOptions.length; i++) {
    qualityOptions[i].addEventListener('click', updateQuality)
}
